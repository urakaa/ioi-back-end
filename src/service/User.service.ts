import { Response, Request, NextFunction } from "express";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

class UserService {
  async allUsers(req: any): Promise<any> {
    const page = parseInt(req.query.page) || 1;
    const limit = parseInt(req.query.limit) || 10;
    const offset = (page - 1) * limit;
    const users = await prisma.user.findMany({
      take: limit,
      skip: offset,
      where: {
        phone: {
          equals: req.query.phone,
        },
        name: {
          contains: req.query.name,
        },
      },
    });
    const totalUsers = await prisma.user.count();
    const totalPages = await Math.ceil(totalUsers / limit);
    return { users: users, totalPages: totalPages, count: totalUsers };
  }

  async test(): Promise<any> {
    const result = await prisma.$queryRawUnsafe(`SELECT password FROM User`);
    return result;
  }

  async deleteUser(id: any): Promise<any> {
    let user = await prisma.user.findUniqueOrThrow({
      where: {
        id: id, // ... provide filter here
      },
    });

    if (!user) {
      return "user not found";
    }

    let result: any = await prisma.user.delete({
      where: {
        id: id,
      },
    });

    return "user deleted";
  }

  async createUser(body: any): Promise<any> {
    let user = await prisma.user.create({
      data: body,
    });
    return user;
  }
}

export default new UserService();
