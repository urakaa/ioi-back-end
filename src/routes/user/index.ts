import { Router } from "express";
import UserController from "../../controller/User.controller";

const router = Router();
router.get("/all", UserController.getAllUsers);
router.post("/cr", UserController.createUser);
router.post("/create", UserController.lai2);
router.post("/update", UserController.updateUser);
router.get("/delete/:userId", UserController.deleteUser);
router.post("/token", UserController.jwtToken);
router.get("/test", UserController.test);

export default router;
