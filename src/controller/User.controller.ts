import { Response, Request, NextFunction } from "express";
import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

class UserController {
  async allUsers(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    let users = await prisma.user.findMany({});
    return res.json(users);
  }

  async createUser(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    let user: any = await prisma.user.create({
      data: {
        name: req.body.name,
        email: req.body.email,
      },
    });

    return res.json(user);
  }

  async updateUser(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    let user: any = await prisma.user.update({
      where: {
        id: req.body.id,
      },
      data: {
        name: req.body.name,
        email: req.body.email,
      },
    });

    return res.json(user);
  }

  async deleteUser(
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<any> {
    let user: any = await prisma.user.delete({
      where: {
        id: req.body.id,
      },
    });

    return res.json("success");
  }
}

export default new UserController();
